


/*Execício 1 - Tabuada de 1 até 10, de 1 até 10 (Consigo fazer em menos de 10 linhas)
O Exercício deve ser feito utilizando For e depois refeito com While

    1 x 1 = 1
    ...
    10 x 1 = 10
    1 x 2 = 2
    ...
    10 x 2 = 20
    1 x 3 = 3
    ...
    10 x 3 = 30

*/



/* Exercício 2 - Imprimir todos os Números pares entre 1 e 100 (Em torno de 5 linhas)
    2
    4
    6
    ...
    100
*/



/* Exercício 3 - Imprimir todos os Números da Sequência de Fibonacci de 1 até 500 (menos de 15 linhas)
    0
    1
    1
    2
    3
    5
    8
    13
    21
    34
    55
    89,
    ...
    N <= 500
*/
