/*
    Laços de Repetição
    É um bloco de código que fica repetindo enquanto a condição do bloco for verdadeira

    While (Enquanto) - É utilizado quando a gente não sabe onde queremos chegar, não sabemos quando algo (condição) irá acabar (ficar falso)

*/


/* Estrutura do Bloco
while (condicaoQueDeveSerVerdadeiraParaEntrarERepetir) {
    //Código
}
*/

//Exemplo básico de contador de 0 até 10
indice = 0
console.log('Início com Índice "'+ indice +'"')
while (indice <= 10) {
    console.log(indice)
    indice += 1
}
console.log('Término com Índice "'+ indice +'"')



//Um exemplo "Real" porque é o caso correto que iremos usar o while, ou seja, quando não sabemos o destino final
var numero = 2156450
var qtdVezesQueImprimiu = 0
while (numero > 1) {
    qtdVezesQueImprimiu += 1
    console.log('Antes da Divisão "' + numero +'"')
    numero /= 5
    console.log('Depois da Divisão "' + numero +'"')
}
console.log('A divisão ocorreu "'+ qtdVezesQueImprimiu +'" vezes')