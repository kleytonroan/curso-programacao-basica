/* Exercício 1
    A Professora quer fazer um app de aprendizado para as crianças praticarem os números par e impar
    Dado um número inicial e um número final, calule dentro do intervalo, quais números são pares e quais são ímpares
    Para isso, use a função Math.round(numero), onde vc passa o número e ela retorna o valor inteiro arredondado em 0 casas decimais

    var numeroInicial = 10
    var numeroFinal = 50
    for (numero = numeroInicial; numero <= numeroFinal; numero++) {
        //Implementação do Código Fonte
    }
    Exemplo
    //Math.round(7 / 2) = 4
    //7 / 2 = 3.5
*/
console.log('Exercício 1');
var numeroInicial = 10;
var numeroFinal = 50;

//Verificando se é verdaeiro ou falso o primeiro número
var isPar = true;
if (Math.round(numeroInicial / 2) !== numeroInicial / 2) {
    isPar = false;
}

var parImpar;
for (var numero = numeroInicial; numero <= numeroFinal; numero++) {
    //Controle de Par ou Ímpar e Impressão
    if (isPar) {
        parImpar = 'Par';
    } else {
        parImpar = 'Impar';
    }
    console.log('O Número "' + numero + '" é "' + parImpar + '"');

    //Lógica de trocar de par para impar e vice-versa
    if (isPar) {
        isPar = false
    } else {
        isPar = true
    }
}
console.log('-----------------------------------------');



/*Exercício (dá para fazer em no máximo 5 linhas)
    Tabuada de 1 a 10, usando for
    1x1 = 1
    2x1 = 2
    ....
    10x1 = 10
    1x2 = 2
    2x2 = 2
    ...
    10x2 = 20
    1x3 = 3
    ...
*/
console.log('Exercício 2');
for (var multiplicador = 1; multiplicador <= 10; multiplicador++) {
    for (var multiplicando = 1; multiplicando <= 10; multiplicando++) {
        console.log(multiplicando + ' x ' + multiplicador + ' = ' + multiplicando * multiplicador);
    }
}
console.log('-----------------------------------------');



/*Exercício 3 (dá para fazer em no máximo 9 linhas)
    Tabuada de 1 a 10, usando while
*/
console.log('Exercício 3');
var multiplicador = 1;
while (multiplicador <= 10) {
    var multiplicando = 1;
    while (multiplicando <= 10) {
        console.log(multiplicando + ' x ' + multiplicador + ' = ' + multiplicando * multiplicador);
        multiplicando += 1
    }
    multiplicador += 1
}
console.log('-----------------------------------------');



/*Exercício 4
    fibonacci
*/
console.log('Exercício 4');
var anterior1 = 0;
var anterior2 = 1;
var resultado;
console.log(anterior1)
console.log(anterior2)
while (anterior1 + anterior2 <= 500) {
    resultado = anterior1 + anterior2
    console.log('->' + resultado)
    anterior2 = anterior1
    anterior1 = resultado
}
console.log('-----------------------------------------');