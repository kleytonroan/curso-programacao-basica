/*
    BOOLEANOS
    Na programação, booleano é verdadeiro (true) ou falso (false)
*/

var verdadeiro = true
console.log(verdadeiro)

var falso = false
console.log(falso)

var resultado = 4 == 2 + 2
console.log('resultado verdadeiro '+ resultado)

var resultado = 4 == 2 + 3
console.log('resultado falso '+ resultado)