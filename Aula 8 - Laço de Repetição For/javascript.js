/*
    Laços de Repetição
    É um bloco de código que fica repetindo enquanto a condição do bloco for verdadeira

    For (Para) - É utilizado quando a gente sabe quando sabemos onde queremos chegar

*/


/* Estrutura do Bloco
for (atribuicaoInicial; condicaoDeRepeticao; valorQueIreiIterarSobreOValorAnterior) {
    //Código
}
*/



//Exemplo Básico - Tabuada de 5
console.log('Início')
for (numerador = 1; numerador <= 10; numerador += 1) {
    console.log(numerador + ' x 5 = ' + numerador * 5);
}
console.log('Fim com numerador "'+ numerador +'"')



console.log('Início')
for (numerador = 1; numerador <= 10; numerador += 3) {
    console.log(numerador + ' x 5 = ' + numerador * 5);
}
console.log('Fim com numerador "'+ numerador +'"')