/*
    NÚMEROS
    Números assim como string, também é a representação da tabela ASC II
*/


var idade = 28
console.log(idade)

var anoNascimento = 1990
console.log(anoNascimento)

var anoAtual = 2019
console.log(anoAtual)

var idade = anoAtual - anoNascimento //subtração
console.log(idade)

var anoAtual = anoNascimento + idade //soma
console.log(anoAtual)
console.log('O Ano Atual é ' + anoAtual)

var divisao = 10 / 2 //divisão
console.log(divisao)

var miltiplicacao = 10 * 2 //multiplicação
console.log(miltiplicacao)

//-------------------------------------------
//Dado a Idade de 28 anos, imprimir 29 aplicando uma soma, e alterando o valor da variavel idade
console.log('---------Desafio 1---------')
idade = 28
console.log(idade + 1)
console.log(idade)

console.log('Resultado')
idade = idade + 1
console.log(idade)

console.log('Continuacao da Aula')
//idade = idade + 1
idade = 28
idade += 1 //idade = idade + 1
console.log(idade)
idade *= 2
console.log(idade)
idade /= 2
console.log(idade)
idade -= 1
console.log(idade)
