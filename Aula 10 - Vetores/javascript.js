
/*
    Vetores (Array) são variáveis especiais que contém mais de um valor ao mesmo tempo
    O Vetor possui posições.
*/

//Declaração
console.log('---------------Declaração');
var carros = [];
console.log(carros);


//Atribuição de Valores
console.log('---------------Atribuição de Valores');
carros[0] = 'fusca';
carros[1] = 'combi';
carros[5] = 'brasilia';
console.log(carros);

carros[6] = 10;
carros[7] = true;
console.log(carros);

//Retornar os Valores
console.log('---------------Retornar Valores');
console.log(carros[1]);


//Exercícios
console.log('---------------Exercício 1');
var cores = [];
cores[0] = 'azul';
cores[1] = 'vermelho';
cores[2] = 'verde';
cores[3] = 'amarelo';
cores[4] = 'rosa';
cores[5] = 'preto';

//Imprimir todas as cores do vetor
console.log(cores.length);
for (posicao = 0; posicao < cores.length; posicao++) {
    console.log(cores[posicao]);
}

console.log('---------------Exercício 2');
//Em que posição está a cor verde
for (posicao = 0; posicao < cores.length; posicao++) {
    if (cores[posicao] === 'verde') {
        console.log(posicao);
    }
}

console.log('---------------Exercício 3');
//Exercício 3 = Passar os valores pares e ímpares para seus respectivos vetores
var numeros = [10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 35, 40, 128];
console.log(numeros);

var ultimaPosicaoPar = 0;
var numerosPares = [];

var ultimaPosicaoImpar = 0;
var numerosImpares = [];

for (posicao = 0; posicao < numeros.length; posicao++) {
    if (numeros[posicao] % 2 === 0) { //par
        numerosPares[ultimaPosicaoPar] = numeros[posicao];
        ultimaPosicaoPar += 1;
    } else { //Ímpar
        numerosImpares[ultimaPosicaoImpar] = numeros[posicao];
        ultimaPosicaoImpar += 1;
    }
}

console.log('Números Pares: ', numerosPares);
console.log('Números Ímpares: ', numerosImpares);



console.log('---------------Exercício 4');
//Execício 4 = Ordenar um Vetor de Números
var troca;
var possuiTrocaPendente = true;
var numeros = [5, 9, 7, 4, 2, 1, 3, 6, 8, 0];
console.log('números misturados: ', numeros);
while (possuiTrocaPendente) {
    possuiTrocaPendente = false;
    for (posicao = 0; posicao < numeros.length - 1; posicao++) {
        if (numeros[posicao] > numeros[posicao + 1]) {
            possuiTrocaPendente = true;
            troca = numeros[posicao];
            numeros[posicao] = numeros[posicao + 1];
            numeros[posicao + 1] = troca;
        }
    }
}
console.log('números ordenados: ', numeros);




console.log('---------------Exercício 5');
//Execício 5 = Ordenar um Vetor de Números
var troca;
var possuiTrocaPendente = true;
numeros = [5, 9, 7, 4, 2, 1, 3, 6, 8, 0];
console.log('números misturados: ', numeros);
while (possuiTrocaPendente) {
    possuiTrocaPendente = false;
    for (posicao = 0; posicao < numeros.length - 1; posicao++) {
        if (numeros[posicao] < numeros[posicao + 1]) {
            possuiTrocaPendente = true;
            troca = numeros[posicao];
            numeros[posicao] = numeros[posicao + 1];
            numeros[posicao + 1] = troca;
        }
    }
}
console.log('números ordenados: ', numeros);