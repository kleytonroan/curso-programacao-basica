/*
    operadores de comparação

    ==      Igual (valor)
    ===     Igual (valor e tipo)
    >       Maior que
    <       Menor que
    >=      Maior ou igual que
    <=      Menor ou igual que
    !       Negação (Not)

    if (contaContabil == 0) {
        error
    }

    if (!contaContabil.IsEmpty()) {
        executa a contabilização
    } else {
        error
    }

    condicional
    basicamente if

*/


/*
    Estrutura Básica do if
    if (true ou false) {

    }


    Estrutura Completa do if
    if (condicao) {

    } else {

    }
*/

//Estrutura Básica
if (true) {
    console.log('verdadeiro basico 1')
    console.log('verdadeiro basico 2')
}



//Estrutura Completa
if (true) {
    console.log('verdadeiro completo 1')
    console.log('verdadeiro completo 2')
} else {
    console.log('falso completo 1')
    console.log('falso completo 2')
}

if (false) {
    console.log('verdadeiro completo 1')
    console.log('verdadeiro completo 2')
} else {
    console.log('falso completo 1')
    console.log('falso completo 2')
}

//Programa verifica menor idade
console.log('Programa verifica menor idade')
var idade = 16
console.log('Minha idade é ' + idade)
if (idade >= 18) {
    console.log('Maior de Idade')
} else {
    console.log('Menor de Idade')
}

var idade = 18
console.log('Minha idade é ' + idade)
if (idade >= 18) {
    console.log('Maior de Idade')
} else {
    console.log('Menor de Idade')
}

var idade = 21
console.log('Minha idade é ' + idade)
if (idade >= 18) {
    console.log('Maior de Idade')
} else {
    console.log('Menor de Idade')
}



//operadores de comparação
console.log(' ')
console.log(' ')
console.log(' ')
console.log('Operador de Comparação ==')
var numero = 5
console.log('Número ' + numero)
if (numero == 5) {
    console.log('Número igual a 5 no valor 1')
}
if (numero == '5') {
    console.log('Número igual a 5 no valor 2')
}


console.log(' ')
console.log(' ')
console.log(' ')
console.log('Operador de Comparação ===')
var numero = 5
console.log('Número ' + numero)
if (numero === 5) {
    console.log('Número igual a 5 no valor e tipo 1')
}
if (numero === '5') {
    console.log('Número igual a 5 no valor e tipo 2')
}


console.log(' ')
console.log(' ')
console.log(' ')
console.log('Operador de Comparação <')
var numero = 5
console.log('Número ' + numero)
if (numero < 5) {
    console.log('Número menor que 5')
} else {
    console.log('Número não é menor que 5')
}
if (numero < 6) {
    console.log('Número menor que 6')
} else {
    console.log('Número não é menor que 6')
}



console.log(' ')
console.log(' ')
console.log(' ')
console.log('Operador de Comparação >')
var numero = 5
console.log('Número ' + numero)
if (numero > 5) {
    console.log('Número maior que 5')
} else {
    console.log('Número não é maior que 5')
}
if (numero > 4) {
    console.log('Número maior que 4')
} else {
    console.log('Número não é maior que 4')
}



console.log(' ')
console.log(' ')
console.log(' ')
console.log('Operador de Comparação >=')
var numero = 5
console.log('Número ' + numero)
if (numero >= 5) {
    console.log('Número maior ou igual que 5')
} else {
    console.log('Número não é maior ou igual que 5')
}
if (numero >= 6) {
    console.log('Número maior ou igual que 6')
} else {
    console.log('Número não é maior ou igual que 6')
}



console.log(' ')
console.log(' ')
console.log(' ')
console.log('Operador de Comparação <=')
var numero = 5
console.log('Número ' + numero)
if (numero <= 5) {
    console.log('Número menor ou igual que 5')
} else {
    console.log('Número não é menor ou igual que 5')
}
if (numero <= 4) {
    console.log('Número menor ou igual que 4')
} else {
    console.log('Número não é menor ou igual que 4')
}



console.log(' ')
console.log(' ')
console.log(' ')
console.log('Operador de Comparação ! (Negação)')
var numero = 5
console.log('Número ' + numero)
if (numero !== 5) {
    console.log('if Número diferente a 5')
} else {
    console.log('else - Número igual 5')
}
if (numero !== 4) {
    console.log('if Número diferente de 4')
} else {
    console.log('else - Número igual a 4')
}



console.log(' ')
console.log(' ')
console.log(' ')
console.log('Mais Exemplos 1 -> !true')
if (!true) {
    console.log('Verdadeiro')
} else {
    console.log('Falso')
}


console.log(' ')
console.log(' ')
console.log(' ')
console.log('Mais Exemplos 2 -> 4 = 2 + 2')
if (!4 == 2 + 2) {
    console.log('Verdadeiro')
} else {
    console.log('Falso')
}



console.log(' ')
console.log(' ')
console.log(' ')
console.log('Mais Exemplos 3 - nome = \'kleyton\'')
nome = 'Kleyton'
if (nome !== 'kleyton') {
    console.log('Nome diferente a kleyton')
} else {
    console.log('Nome igual de kleyton')
}