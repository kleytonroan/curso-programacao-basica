/* Exercício 1
    A Professora quer fazer um app de aprendizado para as crianças praticarem os números par e impar
    Dado um número inicial e um número final, calule dentro do intervalo, quais números são pares e quais são ímpares
    Para isso, use a função Math.round(numero), onde vc passa o número e ela retorna o valor inteiro arredondado em 0 casas decimais

    var numeroInicial 10
    var numeroFinal 50
    for (numero = numeroInicial; numero <= numeroFinal; numero++) {
        //Implementação do Código Fonte
    }
    Exemplo
    //Math.round(7 / 2) = 4
    //7 / 2 = 3.5
*/



/*Exercício (dá para fazer em no máximo 5 linhas)
    Tabuada de 1 a 10, usando for
    1x1 = 1
    2x1 = 2
    ....
    10x1 = 10
    1x2 = 2
    2x2 = 2
    ...
    10x2 = 20
    1x3 = 3
    ...
*/



/*Exercício 3 (dá para fazer em no máximo 7 linhas)
    Tabuada de 1 a 10, usando while
*/