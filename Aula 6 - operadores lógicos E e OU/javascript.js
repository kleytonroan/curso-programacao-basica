
/*

    Operadores Lógicos
    E (&& - And) e o OU (|| - Or)

*/

//AND - Verdadeiro
//Login e Senha
console.log('Explicando o AND (&&) - If com condição verdadeira');
var login = 'kleyton.roan';
var senha = '123456';
if (login === 'kleyton.roan' && senha === '123456') {
    console.log('Você está logado com o Login "'+ login +'" e com a Senha "'+ senha +'"!');
} else {
    console.log('Login "'+ login +'" ou Senha "'+ senha +'" errado!');
}



//AND - Falso
//Login e Senha
console.log('Explicando o AND (&&) - If com condição falsa');
var login = 'kleyton.roan';
var senha = '1234567';
if (login === 'kleyton.roan' && senha === '123456') {
    console.log('Você está logado com o Login "'+ login +'" e com a Senha "'+ senha +'"!');
} else {
    console.log('Login "'+ login +'" ou Senha "'+ senha +'" errado!');
}



//Or - Verdadeiro
//Área restrita para animais domésticos
console.log('Explicando o Or (||) - If com condição verdadeira');
var pet = 'gato';
if (pet === 'cachorro' || pet === 'gato') {
    console.log('Você pode entrar no AP com o Pet "'+ pet +'"');
} else {
    console.log('Você NÃO pode entrar no AP com o Pet "'+ pet +'"');
}



//Or - Falso
//Área restrita para animais domésticos
console.log('Explicando o Or (||) - If com condição falsa');
var pet = 'papagaio';
if (pet === 'cachorro' || pet === 'gato') {
    console.log('Você pode entrar no AP com o Pet "'+ pet +'"');
} else {
    console.log('Você NÃO pode entrar no AP com o Pet "'+ pet +'"');
}



//Misturando AND e OR
//Programa de maior idade (antigamente)
console.log('rograma de maior idade (antigamente) - If com condição verdadeira');
var sexo = 'N'; //M Masculino - F Feminino - N Não binário
var idade = 16;
if (
    (sexo === 'M' && idade >= 18)
    || (sexo === 'F' && idade >= 21)
    || (sexo === 'N' && idade >= 18)
) {
    console.log('Você é maior de idade com o Sexo "'+ sexo +'" e idade "'+ idade +'"');
} else {
    console.log('Você é menor de idade com o Sexo "'+ sexo +'" e idade "'+ idade +'"');
}
