Este repositório é dedicado a um curso básico de programação JavaScript para entender os conceitos da programação antes de entrar no Genexus, uma ferramente Case, onde se programa em Genexus e ele converte o fonte Genexus para outras linguagens.

Para baixar o código fonte, primeiramente deve-se intalar o git, tutoriasis existem de monte na internet, mas basicamente acessar o site do [Git](https://git-scm.com/) que já tem o link pro Download log na página inicial. Instalar com Next, Next Finish, deixando todas as opções no padrão.

Após instalar o Git, executar o seguinte comando: git clone https://kleytonroan@bitbucket.org/kleytonroan/curso-programacao-basica.git CursoProgramacaoBasica

E pronto, vocês terão acesso a todos os Arquivos Fonte do curso, para atualizar os arquivos conforme eu for disponibilizando, executar o comando: git pull, e pronto, os arquivos serão atualizados, lembre-se, não altere os arquivos deste código fonte, ele deve ser usado apenas para consultas.