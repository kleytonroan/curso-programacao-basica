//Correção do Exercício de Par ou Ímpar sem usar o Mod
console.log('Correção do Exercício de Par ou Ímpar sem usar o Mod');
var par = true;
for (var indice = 0; indice <= 50; indice++) {
    if (par) {
        console.log(indice);
    }
    par = !par;
}



//Correção do Exercício de Par ou Ímpar usando o Mod
console.log('Correção do Exercício de Par ou Ímpar usando o Mod');
for (var indice = 0; indice <= 50; indice++) {
    if (indice % 2 === 0) {
        console.log(indice);
    }
}



//Correção do Par ou Ímpar implementando mais regras
//  Regra 1 - Não pode imprimir os números pares entre 10 e 20, ou seja, ... 6, 8, 22, 24, ...
console.log('Correção do Par ou Ímpar implementando mais regras');
var par = true;
var imprimir = true;
for (var indice = 0; indice <= 50; indice++) {

    //Regra de Negócio
    if (par && imprimir) {
        console.log(indice);
    }

    //Controles
    par = !par;
    if (indice + 1 >= 10 && indice + 1 <= 20) {
        imprimir = false;
    } else {
        imprimir = true;
    }

}